#!/bin/bash

# May 2016 Borja Aparicio
# Workaround for https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/196

function docker_active_containers () {

        containers=$(docker ps | wc -l)
        if [ "$containers" -eq "1" ]
        then
                echo false
        else
                echo true
        fi

}

function restart_docker () {

        any_active=$(docker_active_containers)
        # No containers running: restart demon
        if [ "$any_active" == "false" ]
        then
                systemctl restart docker.service
                exit 0
        fi

        # Container running: wait a minute
        sleep 60
        any_active=$(docker_active_containers)
        if [ "$any_active" == "false" ]
        then
                systemctl restart docker.service
                exit 0
        fi

        # Container running: wait five minutes
        sleep 300
        any_active=$(docker_active_containers)
        if [ "$any_active" == "false" ]
        then
                systemctl restart docker.service
                exit 0
        fi

        # Giving up. Cron will run again
        exit 0

}

lsof="/usr/sbin/lsof"
if [ ! -x $lsof ]
then
        echo ERROR: $lsof not found
        exitch user and  1
fi

wc="/usr/bin/wc"
if [ ! -x $wc ]
then
        echo ERROR: $wc not found
        exit 1
fi

grep_bin="/usr/bin/grep"
if [ ! -x $grep_bin ]
then
        echo ERROR: $grep_bin not found
        exit 1
fi

number_of_docker_sockets=$($lsof | $grep_bin docker.sock | wc -l)

if [ "$number_of_docker_sockets" -gt "50000" ]
then
        restart_docker
fi

